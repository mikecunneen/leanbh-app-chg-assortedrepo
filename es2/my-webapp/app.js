

var express = require('express');
var stormpath = require('express-stormpath');

var app = express();

app.set('views', './views');
app.set('view engine', 'jade');

var stormpathMiddleware = stormpath.init(app, {
  apiKeyFile: '/.stormpath/apiKey.properties',
  application: 'https://api.stormpath.com/v1/applications/7QJccMTCXiHIGnJUqSvoCC',
  secretKey: '&@L4nGH8hg-DuAfL9^Eb5"yugPug?&ja^5u7k>){',
  expandCustomData: true,
  enableForgotPassword: true
});

app.use(stormpathMiddleware);

app.get('/', function(req, res) {
  res.render('home', {
    title: 'Welcome'
  });
});

app.listen(3000);
