package com.example.btsample;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity {
	
	public static DataInputStream BT_Clientdis;//藍芽資料輸入
	public static DataOutputStream BT_Clientdos;//藍芽資料輸出
	ArrayList<Byte> buff;
	public static final byte CMD_HBP16_CHK_Afib[] =  {(byte)0x12,(byte)0x16,0x18,(byte)0xf0,0x00,0x00,0x00};
	public static final byte CMD_HBP16_BP_READ[] = {(byte)0x12,(byte)0x16,0x18, 0x28};
	
	public static String DeviceType = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		try {
			
		checkAfib();
		
		byte dump;
		buff=new ArrayList<Byte>();
		seedCMD(CMD_HBP16_BP_READ);
		int breakpoint=0;
		int datasize=0;
		boolean isclear = false;
		
		//SharedData.BT_Clientdis
		while( (dump = BT_Clientdis.readByte()) != -1){
			 
			 //Log(buff.size()+":"+String.format("%02X", (int)(dump&0xFF)));
			 if(dump==0x06){
				 isclear=true;
				 Log.i("BTSocketReadThread", "dump 0x06");
				 //break;
			 }
			 if(isclear==true){
				 buff.add(dump);
			 }
			 if(buff.size()==2){
				 datasize=(int)((buff.get(1)-0x01)&0xFF);
				 Log.i("ReadThread datasize", datasize +"" );
				 breakpoint=8+(datasize*7)+8;
				 //Log.i("BTSocketReadThread", bkPos +"");
				 Log.i("BTSocketReadThread", String.format("%02X", (int)(dump&0xFF)));
				 //break;
			 }
			 if(buff.size()>2&&breakpoint==buff.size()){
				 byte total=0x00;
				 for(int x=1;x<buff.size();x++){
					 total^=buff.get(x);
				 }
				 Log.i("BTSocketReadThread", String.format("%02X", (int)(total&0xFF)));
				 break;
			 }

		}
		//SharedData.BT_Clientdis.readByte();
		Log.i("BTSocketReadThread", "readByte");
		
		ArrayList<Byte> temp = new ArrayList<Byte>();
		if(buff.size()>3){
			for(int b=8;b<(buff.size()-8);b++){
				temp.add(buff.get(b));
				if(temp.size()==7){
					if(DeviceType.equalsIgnoreCase("51")){
						ParseByteHomeAAvg2(temp);
					}
					else{
						ParseByte7(temp);
					}
					temp.clear();
				}
			}
			temp.clear();
			for(int b=(buff.size()-8);b<(buff.size()-1);b++){
				temp.add(buff.get(b));
				if(temp.size()==7){
					ParseByteAvg(temp);
					temp.clear();
				}
			}
		}
		
		//sync time
		syncDataTime();
		
		//return 0;
		} catch (IOException e) {
			e.printStackTrace();
			//return 1;
		}finally{
			//db.close();
		}
		
	}
	
	
	private void ParseByte7(ArrayList<Byte> rec){
		String sp=String.format("%02d", (int)(rec.get(0)&0xFF));	
		String dp=String.format("%02d", (int)(rec.get(1)&0xFF));
		String hp=String.format("%02d", (int)(rec.get(2)&0xFF));
		String year=String.format("%02d", (int)(rec.get(3)&0xF0)>>4 );
		String month=String.format("%02d", (int)(rec.get(3)&0x0F) );
		String day=String.format("%02d", (int)((rec.get(4)&0xF8))>>3);
		String hour=String.format("%02d", (int)((rec.get(4)&0x07)<<2)+(int)((rec.get(5)&0xC0)>>6));
		String min=String.format("%02d", (int)(rec.get(5)&0x3F));
	
		String date="20%s/%s/%s";
		String time="%s:%s:00";
		date=String.format(date, year,month,day);
		time=String.format(time, hour,min);
		//SaveHistory( date, time, sp, dp, hp); //儲存紀錄
	}
	private void ParseByteHomeAAvg2(ArrayList<Byte> rec){
		String sp=String.format("%02d", (int)(rec.get(0)&0xFF));	
		String dp=String.format("%02d", (int)(rec.get(1)&0xFF));
		String hp=String.format("%02d", (int)(rec.get(2)&0xFF));
		String year=String.format("%02d", (int)(rec.get(3)&0xF0)>>4 );
		String month=String.format("%02d", (int)(rec.get(3)&0x0F) );
		String day=String.format("%02d", (int)((rec.get(4)&0xF8))>>3);
		String hour=String.format("%02d", (int)((rec.get(4)&0x07)<<2)+(int)((rec.get(5)&0xC0)>>6));
		String min=String.format("%02d", (int)(rec.get(5)&0x3F));
		int afib=(int)(rec.get(6)&0x01<<2);
		
		String afibStr = "0";
		
		if (afib==1){
			afibStr="1";
		}
		
		String date="20%s/%s/%s";
		String time="%s:%s:00";
		date=String.format(date, year,month,day);
		time=String.format(time, hour,min);
		//SaveHomeAHistory( date, time, sp, dp, hp,afibStr);//儲存紀錄
	}
	private void ParseByteAvg(ArrayList<Byte> rec){
		String cur_wbp_sp=String.format("%02d", (int)(rec.get(0)&0xFF));	
		String cur_wbp_dp=String.format("%02d", (int)(rec.get(1)&0xFF));
		String cur_wbp_hp=String.format("%02d", (int)(rec.get(2)&0xFF));
	}

	//判斷是否有開啟AFIB
	private void checkAfib(){
		byte dump;
		buff=new ArrayList<Byte>();
		seedCMD(CMD_HBP16_CHK_Afib);
		String stringOfbuff="";
		boolean isstart = false;
		
		try {
		while( (dump = BT_Clientdis.readByte()) != -1){
			 
			// Log(buff.size()+":"+String.format("%02X", (int)(dump&0xFF)));
			 if( isstart==true){ 
				 buff.add(dump);
			 }
			 if(dump==0x06){
				// Log.i("BTSocketReadThread", "dump 0x06");
				 //break;
				 isstart=true;
				 
			 }
			 if(buff.size()==12){
				 stringOfbuff = String.format("%02X", (int)(buff.get(11)&0xFF));
				 if(stringOfbuff.equalsIgnoreCase("8D")){
					 DeviceType = "51";
				 }
				 else{					 
					 DeviceType = "4C";
				 }
				 break;
			 }
		 }

		}
		catch (IOException e) {
			e.printStackTrace();
		}finally{
			
		}
	}
			
	private void seedCMD(byte[] data){
		try {
			BT_Clientdos.write(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void syncDataTime(){
		String formatStr = "%02x";
				
	    Calendar c=Calendar.getInstance();
	
		String year=String.format(formatStr, c.get(Calendar.YEAR)-2000);
		String month=String.format(formatStr, c.get(Calendar.MONTH)+1);
		String day=String.format(formatStr, c.get(Calendar.DAY_OF_MONTH));
		String hour=String.format(formatStr, c.get(Calendar.HOUR_OF_DAY));
		String minute=String.format(formatStr, c.get(Calendar.MINUTE));
		String second=String.format(formatStr, c.get(Calendar.SECOND));
		
		String hexString="4DFF0805" + year + month +day + hour + minute + second;
								
		byte[] CMD_HBP16_BP_DateTime=hexStringToByteArray(hexString);
		int checkSum = 0;
		  for(byte b : CMD_HBP16_BP_DateTime){
		    checkSum += (0xff & b);
		  }
		String checkSumHex = Integer.toHexString(checkSum & 0xFF);
		byte[] checkSum_HBP16=hexStringToByteArray(checkSumHex);
		
		byte[] CMD_HBP16_BP_SyncDateTime=concatenateByteArrays(CMD_HBP16_BP_DateTime,checkSum_HBP16);
		//byte CMD_HBP16_BP_SetDateTime[] = {(byte)0x4D,(byte)0xFF,0x08,0x05 ,(byte)0x0D,0x09,0x10,0x0E,0x0D,0x25,(byte)0xBF};			
		seedCMD(CMD_HBP16_BP_SyncDateTime);		
	}
	
	private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len/2];

        for(int i = 0; i < len; i+=2){
            data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }
	
	private byte[] concatenateByteArrays(byte[] a, byte[] b) {
	    byte[] result = new byte[a.length + b.length]; 
	    System.arraycopy(a, 0, result, 0, a.length); 
	    System.arraycopy(b, 0, result, a.length, b.length); 
	    return result;
	} 
}
